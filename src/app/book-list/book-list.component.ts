import { Component, OnInit } from '@angular/core';
import {BookService} from '../service/book.service';
import {FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  books;

  addForm;

  updateForm;

  isShownUpdateForm = false;

  updateId;

  updateTitle;

  constructor(private bookService: BookService,
              private formBuilder: FormBuilder) {
    this.addForm = this.formBuilder.group({
      title: '',
      author: '',
      editionDate: '',
      price: ''
    });
    this.updateForm = this.formBuilder.group({
      price: ''
    });
  }

  getBooks() {
    this.bookService.getBooks()
      .subscribe(results => this.books = results);
  }

  addBook(newBook) {
    this.bookService.addBook(newBook)
      .subscribe(() => {
        this.getBooks();
        this.addForm.reset();
      });
  }

  showUpdateForm(id: number, title: string) {
    this.isShownUpdateForm = true;
    this.updateId = id;
    this.updateTitle = title;
  }

  updateBook(id: number, modifiedBook) {
    this.bookService.updateBook(id, modifiedBook)
      .subscribe(() => {
        this.updateForm.reset();
        this.isShownUpdateForm = false;
        this.getBooks();
      });
  }

  deleteBook(id: number) {
    this.bookService.deleteBook(id)
      .subscribe(() => this.getBooks());
  }

  ngOnInit() {
    this.getBooks();
  }

  onSubmit(customerData, opr) {
    if (opr === 'add') {
      this.addBook(customerData);
    } else {
      this.updateBook(this.updateId, customerData);
    }
  }

}
