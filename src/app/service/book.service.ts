import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http: HttpClient) { }

  booksUrl = 'http://localhost:8080/books';
  addBookUrl = 'http://localhost:8080/add-book';
  updateBookUrl = 'http://localhost:8080/update-book';
  deleteBookUrl = 'http://localhost:8080/delete-book';

  getBooks() {
    return this.http.get(this.booksUrl);
  }

  addBook(book) {
    return this.http.post(this.addBookUrl, book);
  }

  updateBook(id: number, book) {
    const url = this.updateBookUrl + '?id=' + id;
    return this.http.put(url, book);
  }

  deleteBook(id: number) {
    const url = `${this.deleteBookUrl}/${id}`;
    return this.http.delete(url);
  }
}
